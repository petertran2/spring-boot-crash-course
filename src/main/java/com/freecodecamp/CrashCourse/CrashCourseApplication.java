package com.freecodecamp.CrashCourse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
// https://youtu.be/r-6BwGW4Sr8?t=2237
@SpringBootApplication
public class CrashCourseApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrashCourseApplication.class, args);
	}

}
